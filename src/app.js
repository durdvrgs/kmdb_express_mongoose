const PORT = 3000
const express = require('express')
const app = express()
const passport = require('passport')
const applyPassportStrategy = require('./loaders/passport')
const bindAPIRoutes = require('./api')


app.use(express.json())

applyPassportStrategy(passport)
bindAPIRoutes(app)

app.listen(PORT, () => {
    console.log(`Listeing on <http:localhost:${PORT}>`)
})