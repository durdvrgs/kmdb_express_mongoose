const { Comments, Movies } = require('../../models')


module.exports.create = async (req, res) => {

    const thisMovie = await Movies.findOne({ _id: req.params['id'] })

    if (!thisMovie) {
        return res.status(404).res({
            "detail": "Movie not found."
        })
    }

    const thisComment = await Comments.create({
        comment: req.body['comment'],
        user: {
            _id: req.user['id'],
            // firstName: req.user['firstName'],
            // lastName: req.user['lastName']
        }
    })

    thisMovie['commentSet'].push(thisComment)
    await thisMovie.save()

    return res.status(201).send(thisComment)
}


module.exports.update = async (req, res) => {

    const thisMovie = await Movies.findOne({ _id: req.params['id'] })

    if (!thisMovie) {
        return res.status(404).send({
            'detail': 'Movie not found.'
        })
    }

    const commentToUpdateId = req.body['commentId']

    thisMovie['commentSet'].forEach( async (commentId) => {

        if (commentId.toString() === commentToUpdateId.toString() ) {

            let thisComment = await Comments.findOne({ _id: commentToUpdateId })

            if (thisComment.user['_id'].toString() === req.user['id'].toString()) {

                thisComment['comment'] = req.body['comment']
                await thisComment.save()
                
                return res.send(thisComment)
            }
            else {

                return res.status(403).send({
                    "detail": "You do not have permission to perform this action."
                    })
            }
        }
    })
}