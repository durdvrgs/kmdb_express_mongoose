const { Movies, Genres } = require('../../models')


module.exports.list = async (_, res) => {

    const moviesList = await Movies.find({}).populate('Reviews')

    res.send(moviesList)
}

module.exports.create = async (req, res) => {

    // const { title, duration, genres, launch, classification, synopsis } = req.body
    const { title, genres } = req.body

    const thisMovieAlreadyExist = await Movies.findOne( { title: title })

    if (thisMovieAlreadyExist) {
        return res.status(401).send({
            'msg': 'Movie already registered'
        })
    }

    genres.forEach( async (genre) => {

        let genreName = genre['name']
        let thisGenre = await Genres.findOne({ name: genreName })

        if (!thisGenre) {
            await Genres.create({ name: genreName })
        }
    })

    const newMovie = await Movies.create(req.body)

    newMovie.genres.forEach( async (genre) => {

        let thisGenre = await Genres.findOne( { name: genre['name']} )
        thisGenre.movies.push(newMovie['_id'])

        await thisGenre.save()
    })

    res.status(201).send(newMovie)
}

module.exports.retrieve = async (req, res) => {

    const thisMovie = await Movies.findOne({ _id: req.params['id'] })

    if (!thisMovie) {
        return res.status(404).send({
            "detail": "Movie not found"
        })
    }

    return res.send(thisMovie)
}

module.exports.destroy = async (req, res) => {

    const thisMovie = await Movies.findOne({ _id: req.params['id'] })

    if (!thisMovie) {
        return res.status(404).send({
            "detail": "Movie not found"
        })
    }

    await thisMovie.deleteOne()

    return res.status(204).send()
}