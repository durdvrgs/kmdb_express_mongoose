const { Reviews, Movies } = require('../../models')


module.exports.create = async (req, res) => {

    const thisMovie = await Movies.findOne({ _id: req.params['id'] })

    if (!thisMovie) {

        return res.status(404).send({
            "detail": "Movie not found."
          })
    }

    thisMovie['critcismSet'].forEach( async (thisCriticId) => {

        let thisReview = await Reviews.findOne({ _id: thisCriticId })
        
        if (thisReview['critict']['_id'].toString() === req.user['id'].toString()) {

            return res.status(422).send(
                    {
                        "detail": "You already made this review."
                    }
            )
        }
    })

    const { stars, review, spoilers } = req.body

    const newReview = await Reviews.create({
        stars: stars,
        review: review,
        spoilers: spoilers,
        critict: {
            _id: req.user['id']
        }
    })

    thisMovie['critcismSet'].push(newReview)
    await thisMovie.save()

    res.status(201).send(newReview)
}

module.exports.update = async (req, res) => {

    const thisMovie = await Movies.findOne({ _id: req.params['id'] })

    if (!thisMovie) {
        return res.status(404).send({
            "detail": "Not found."
          })
    }

    thisMovie['critcismSet'].forEach( async () => {

        let thisReview = await Reviews.findOne({ critict: req.user['id'] })

        if (thisReview) {

            Object.keys(req.body).forEach( (key) => {
                thisReview[key] = req.body[key]
            })

            await thisReview.save()

            return res.send(thisReview)
        }
        else {

            return res.status(404).send({
                "detail": "Not found."
              })
        }
    })
}