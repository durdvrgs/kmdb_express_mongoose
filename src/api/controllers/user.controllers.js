const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const config = require('../../config/passport')

const { Users } = require('../../models')


module.exports.signup = async (req, res) => {

    const userNameAlreadyExist = await Users.findOne({ userName: req.body['userName']})

    if (userNameAlreadyExist) {
        return res.status(401).send({
            'detail': 'Username already registered'
        })
    }

    const { userName, firstName, lastName, password, isSuperuser, isStaff } = req.body

    const hashPassword = bcrypt.hashSync(password, bcrypt.genSaltSync(9))


    const newUser = await Users.create({
        userName: userName,
        firstName: firstName,
        lastName: lastName,
        password: hashPassword,
        isSuperuser: isSuperuser,
        isStaff: isStaff
    })

    res.status(201).send({
        firstName: newUser.firstName,
        lastName: newUser.lastName,
        isSuperuser: newUser.isSuperuser,
        isStaff: newUser.isStaff
    })
}

module.exports.signin = async (req, res) => {

    const data = req.body

    const thisUser = await Users.findOne( { userName: data['userName'] } )

    if (!thisUser) {

        res.status(404).send({
            "msg": "userName not Found"
        })
    }

    const validatePassword = await bcrypt.compare(data['password'], thisUser['password'])

    if (validatePassword) {

        const TOKEN = jwt.sign( { userName: thisUser['userName'] }, config.passport.secret)

        return res.send({ TOKEN })
    }

    return res.status(401).send( { message: 'Invalid password' })
}