const bindUserRoutes = require('./routes/user.routes')
const bindMovieRoutes = require('./routes/movie.routes')
const bindReviewRoutes = require('./routes/review.routes')
const bindCommentRoutes = require('./routes/comment.routes')

module.exports = (app) => {

    bindUserRoutes(app)
    bindMovieRoutes(app)
    bindReviewRoutes(app)
    bindCommentRoutes(app)
}
