module.exports.isSuperUser = (req, res, next) => {

    const user = req.user

    if (user.isSuperuser) {
        return next()
    }

    return res.status(403).json({
        detail: "You do not have permission to perform this action."
      })
}