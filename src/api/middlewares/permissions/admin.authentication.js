module.exports.isAdmin = (req, res, next) => {

    const user = req.user

    if (user.isStaff && user.isSuperuser) {
        return next()
    }

    return res.status(403).json({
        detail: "You do not have permission to perform this action."
      })
}