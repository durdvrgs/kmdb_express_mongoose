module.exports.isCommentUser = (req, res, next) => {

    const user = req.user

    if (!user.isSuperuser && !user.isStaff) {
        return next()
    }

    return res.status(403).json({
        detail: "You do not have permission to perform this action."
      })
}