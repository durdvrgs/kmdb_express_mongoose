const passport = require('passport')

module.exports.tokenAuthentication = (req, res, next) => {
   
    passport.authenticate('jwt', { session: false } )(req, res, next);
}