const { body } = require('express-validator')

module.exports = () => {

    return [
        body('comment')
        .isString().withMessage('This field should be a String')
        .notEmpty().withMessage('This field is required')
    ]
}