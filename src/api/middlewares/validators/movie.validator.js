const { body } = require('express-validator')

module.exports = () => {

    return [
        body('title')
        .isString().withMessage('This field should be a String')
        .notEmpty().withMessage('This field is required'),

        body('duration')
        .isString().withMessage('This field should be a String')
        .notEmpty().withMessage('This field is required'),

        body('genres')
        .isArray().withMessage('This field should be a List')
        .notEmpty().withMessage('This field is required'),

        body('launch')
        .isDate().withMessage('This field should be a Date')
        .notEmpty().withMessage('This field is required'),

        body('classification')
        .isInt({min: 0}).withMessage('This field should be a Integer')
        .notEmpty().withMessage('This field is required'),

        body('synopsis')
        .isString().withMessage('This field should be a String')
        .notEmpty().withMessage('This field is required')
    ]
}