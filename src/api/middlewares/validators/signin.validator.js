const { body } = require('express-validator');

module.exports = () => {
    return [
        body('userName')
        .isString().withMessage('This field should be a String')
        .notEmpty().withMessage('This field is required'),

        body('password')
        .isString().withMessage('This field should be a String')
        .notEmpty().withMessage('This field is required')
    ]
}