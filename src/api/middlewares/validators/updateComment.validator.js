const { body } = require('express-validator')


module.exports = () => {

    return [
        body('comment')
        .isString().withMessage('This field should be a String')
        .notEmpty().withMessage('This field is required'),

        body('commentId')
        .isString().withMessage('This field should be a User Token')
        .notEmpty().withMessage('This field is required')
    ]
}