const { body } = require('express-validator')

module.exports = () => {

    return [
        body('stars')
        .isInt({min: 1, max: 10}).withMessage('Thid field should be a Integer, min: 1 and max: 10'),

        body('review')
        .isString().withMessage('This field should be a String')
        .notEmpty().withMessage('This field is required'),

        body('spoilers')
        .isBoolean().withMessage('This field should be a Boolean')
        .notEmpty().withMessage('This field is required')
    ]

}