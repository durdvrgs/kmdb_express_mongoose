const { body } = require('express-validator');

module.exports = () => {
    return [
        body('userName')
        .isString().withMessage('This field should be a String')
        .notEmpty().withMessage('This field is required'),

        body('password')
        .isString().withMessage('This field should be a String')
        .notEmpty().withMessage('This field is required'),

        body('firstName')
        .isString().withMessage('This field should be a String')
        .notEmpty().withMessage('This field is required'),

        body('lastName')
        .isString().withMessage('This field should be a String')
        .notEmpty().withMessage('This field is required'),

        body('isSuperuser')
        .isBoolean().withMessage('This field should be a Boolean')
        .notEmpty().withMessage('This field is required'),

        body('isStaff')
        .isBoolean().withMessage('This field should be a Boolean')
        .notEmpty().withMessage('This field is required')
    ]
}