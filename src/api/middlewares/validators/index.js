const { validationResult } = require('express-validator');

module.exports.signinValidations = require('./signin.validator')
module.exports.signupValidations = require('./signup.validator')
module.exports.movieValidations = require('./movie.validator')
module.exports.reviewValidations = require('./review.validator')
module.exports.commentValidations = require('./comment.validator')
module.exports.updateCommentValidations = require('./updateComment.validator')

module.exports.validate = (req, res, next) => {

    const errors = validationResult(req)

    if (errors.isEmpty()) {
        return next()
    }

    const extractedErrors = []
    errors.array().map( (err) => extractedErrors.push({ [err.param]: err.msg }))

    return res.status(422).json({
        errors: extractedErrors
    })
}