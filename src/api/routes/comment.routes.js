const { Router } = require('express')
const router = Router()


module.exports = (app) => {

    const { tokenAuthentication } = require("../middlewares/permissions/tokenAuthentication")
    const { isCommentUser } = require('../middlewares/permissions/user.authentication')
    const { commentValidations, updateCommentValidations, validate } = require('../middlewares/validators')
    const { create, update } = require('../controllers/comment.controller')

    
    app.use('/api/movies/:id/comments', tokenAuthentication)

    router.post('/movies/:id/comments', commentValidations(), validate, isCommentUser, create)
    router.put('/movies/:id/comments', updateCommentValidations(), validate, isCommentUser, update)

    app.use('/api', router)
}