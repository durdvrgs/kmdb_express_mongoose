const { Router } = require('express')
const router = Router()


module.exports = (app) => {

    const { validate, signinValidations, signupValidations } = require('../middlewares/validators')
    const { signup, signin } = require('../controllers/user.controllers')

    router.post('/accounts', signupValidations(), validate, signup)
    router.post('/login', signinValidations(), validate, signin)

    app.use('/api', router)
}