const { Router } = require('express')
const router = Router()


module.exports = (app) => {

    const { isAdmin } = require('../middlewares/permissions/admin.authentication')
    const { validate, movieValidations } = require('../middlewares/validators')
    const { tokenAuthentication } = require('../middlewares/permissions/tokenAuthentication')
    const { list, create, retrieve, destroy } = require('../controllers/movie.controllers')


    app.use('/api/movies', tokenAuthentication)
    app.use('/api/movies/:id', tokenAuthentication)

    router.get('/movies', isAdmin, list)
    router.post('/movies', movieValidations(), validate, isAdmin, create)
    router.get('/movies/:id', isAdmin, retrieve)
    router.delete('/movies/:id', isAdmin, destroy)

    app.use('/api', router)
}