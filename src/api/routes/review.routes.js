const { Router } = require('express')
const router = Router()

module.exports = (app) => {

    const { tokenAuthentication } = require('../middlewares/permissions/tokenAuthentication')
    const { isStaffUser } = require('../middlewares/permissions/staff.authentication')
    const { validate, reviewValidations } = require('../middlewares/validators')
    const { create, update } = require('../controllers/review.controllers')

    app.use('/api/movies/:id/review', tokenAuthentication)

    router.post('/movies/:id/review', reviewValidations(), validate, isStaffUser, create)
    router.put('/movies/:id/review', reviewValidations(), validate, isStaffUser, update)
    // router.post('/movies/:id/review', create)

    app.use('/api', router)
}