module.exports = (mongoose) => {

    const { Schema } = mongoose

    const genresSchema = new Schema ({

        name: {
            type: Schema.Types.String,
            required: true,
            unique: true
        },

        movies: [
            { type: Schema.Types.ObjectId, ref: 'Movies' }
        ]
    })

    mongoose.model("Genres", genresSchema)

    return "Genres"
}