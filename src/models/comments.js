module.exports = (mongoose) => {

    const { Schema } = mongoose

    const commentSchema = new Schema ({

        comment: {
            type: Schema.Types.String,
            required: true
        },

        user: {
            type: Schema.Types.ObjectId, ref: 'Users'
        }
    })

    mongoose.model("Comments", commentSchema)

    return "Comments"
}