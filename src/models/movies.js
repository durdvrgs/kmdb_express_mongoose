module.exports = (mongoose) => {

    const { Schema } = mongoose

    const moviesSchema = new Schema ({

        title: {
            type: Schema.Types.String,
            require: true
        },

        duration: {
            type: Schema.Types.String,
            require: true
        },

        genres: [
            { name: { type: Schema.Types.String, ref: 'Genres' } }
        ],

        launch: {
            type: Schema.Types.Date,
            require: true
        },

        classification: {
            type: Schema.Types.Number,
            require: true
        },

        synopsis: {
            type: Schema.Types.String,
            require: true
        },

        critcismSet: [
            { type: Schema.Types.ObjectId, ref: 'Users' }
        ],

        commentSet: [
            { type: Schema.Types.ObjectId, ref: 'Users' }
        ],
    })

    mongoose.model("Movies", moviesSchema)

    return "Movies"
}