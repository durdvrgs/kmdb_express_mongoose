module.exports = (mongoose) => {

    const { Schema } = mongoose

    const reviewSchema = new Schema ({

        stars: {
            type: Schema.Types.Number,
            require: true,
            min: 1,
            max: 10
        },

        review: {
            type: Schema.Types.String,
            require: true,
        },

        spoilers: {
            type: Schema.Types.Boolean,
            required: true,
        },

        critict: {
            type: Schema.Types.ObjectId,
            ref: "Users",
            required: true
        }
    })

    mongoose.model("Reviews", reviewSchema)

    return "Reviews"
}