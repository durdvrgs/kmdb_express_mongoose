module.exports = (mongoose) => {

    const { Schema } = mongoose

    const userSchema = new Schema ({

        userName: {
            type: Schema.Types.String,
            required: true,
            unique: true
        },

        firstName: {
            type: Schema.Types.String,
            required: true,
        },

        lastName: {
            type: Schema.Types.String,
            required: true
        },

        password: {
            type: Schema.Types.String,
            required: true
        },

        isSuperuser: {
            type: Schema.Types.Boolean,
            required: true
        },

        isStaff: {
            type: Schema.Types.Boolean,
            required: true
        }
    })

    mongoose.model("Users", userSchema)

    return "Users"
}
