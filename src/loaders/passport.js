const { Strategy, ExtractJwt } = require('passport-jwt');
const config = require('../config/passport');

const { Users } = require('../models');

module.exports = (passport) => {
    const options = {};

    options.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();

    options.secretOrKey = config.passport.secret;

    const strategy = new Strategy(options, async (payload, done) => {

        const user = await Users.findOne({ userName: payload.userName });

        if (!user) return done({ error: 'Failed to load user'}, false);

        return done(null, {
            id: user._id,
            userName: user.userName,
            firstName: user.firstName,
            lastName: user.lastName,
            isSuperuser: user.isSuperuser,
            isStaff: user.isStaff,
        });
    });

    passport.use(strategy);
}
