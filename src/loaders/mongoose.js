const mongoose = require('mongoose');
const config = require('../config');

module.exports = async () => {
    console.log(config.databaseURL);
    const connection = await mongoose.connect('mongodb://localhost:27017/kmdb_express', {
        auth: { 'authSource': 'admin' },
        user: 'admin',
        pass: 'admin',
        useNewUrlParser: true,
        useUnifiedTopology: true
    });
    return connection.connection.db;
}